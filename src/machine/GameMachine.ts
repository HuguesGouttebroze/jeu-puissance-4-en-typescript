import { actions, interpret, InterpreterFrom } from 'xstate'
import { GameContext, GameStates, GridState, Player, PlayerColor, Position } from '../types'
import {
  canChooseColorGuard,
  canDropGuard,
  canJoinGuard,
  canLeaveGuard,
  canStartGameGuard, isDrawMoveGuard,
  isWiningMoveGuard
} from './guards'
import {
  chooseColorAction,
  dropTokenAction,
  joinGameAction,
  leaveGameAction, restartAction,
  saveWiningPositionsActions,
  setCurrentPlayerAction,
  switchPlayerAction
} from './actions'
import { createMachine } from "xstate";
import { createModel } from "xstate/lib/model";


// create "enum" types (rather than string)
//enum GameStates {
    // define differents states
   // LOBBY = "LOBBY", // player are waiting
   // PLAY = "PLAY", // players are playing game
   // VICTORY = "VICTORY", // win if 4 pings are alligns
   // DRAW  = "DRAW", // if there's no winner when aff the grid is used 
//}

export const GameModel = createModel({
    // define the model context
    players: [] as Player[],
    currentPlayer: null as null | Player['id'],
    rowLength: 4,
    winingPositions: [] as Position[],
    grid: [
        ["E", "E", "E", "E", "E", "E", "E"],
        ["E", "E", "E", "E", "E", "E", "E"],
        ["E", "E", "E", "E", "E", "E", "E"],
        ["E", "E", "E", "E", "E", "E", "E"],
        ["E", "E", "E", "E", "E", "E", "E"],
        ["E", "E", "E", "E", "E", "E", "E"],
    ] as GridState // take a look to the types.ts file that simplify this grid system & thes states comes from 
  }, {
    events: {
        join: (playerId: Player["id"], name: Player["name"]) => 
        ({playerId, name}),// allow to create an EVENT with a `GameModel.events.join()` and to know params and also to generate the events from the GameModel (see type file)
        leave: (playerId: Player["id"]) => ({playerId}),
        chooseColor: (playerId: Player["id"], color: PlayerColor) => 
        ({playerId, color}),
        start: (playerId: Player["id"]) => ({playerId}),  
        dropToken: (playerId: Player["id"], x: number) => ({playerId, x}),
        restart:  (playerId: Player["id"]) => ({playerId}), // to restart game
    }
});

export const GameMachine = GameModel.createMachine({
    // define transitions to move from states
    // init the state
    id: "game",
    /* context: GameModel.initialContext, */
    context: {
        ...GameModel.initialContext, 
        players: [{
            id: 'Zoidberg',
            name: 'Zoidberg', 
            color: PlayerColor.YELLOW
        }, {
            id: 'QwickQwack',
            name: 'QwickQwack',
            color: PlayerColor.RED
        }],
        currentPlayer: 'Zoidberg'
    },
    initial: GameStates.PLAY,
    states: { 
        // LOBBY's state transitions
        [GameStates.LOBBY]: {
            on: {
                join: { // reafect to a new partie, so it make come back to the state begin LOBBY, we don't move of our state
                    cond: canJoinGuard,
                    actions: [GameModel.assign(joinGameAction)],
                    target: GameStates.LOBBY
                },
                chooseColor: { // stay to LOBBY state
                    cond: canChooseColorGuard,
                    target: GameStates.LOBBY,
                    actions: [GameModel.assign(chooseColorAction)]
                },
                leave: {
                    cond: canLeaveGuard,
                    actions: [GameModel.assign(leaveGameAction)],
                    target: GameStates.LOBBY
                },
                start: { // start & play directly
                    cond: canStartGameGuard, 
                    target: GameStates.PLAY, 
                    actions: [GameModel.assign(setCurrentPlayerAction)]
                }
            } 
        },
        // PLAY's state transitions
        [GameStates.PLAY]: {
            after: {
                15000: {
                    target: GameStates.PLAY,
                    actions: [GameModel.assign(switchPlayerAction)]
                }
            },
            on: {
                dropToken: [
                { 
                    cond: isDrawMoveGuard,
                    target: GameStates.DRAW,
                    actions: [GameModel.assign(dropTokenAction)]
                },
                {
                    cond: isWiningMoveGuard,
                    target: GameStates.VICTORY,
                    actions: [GameModel.assign(saveWiningPositionsActions), GameModel.assign(dropTokenAction)]
                },
                {
                    cond: canDropGuard,
                    target: GameStates.PLAY,
                    actions: [GameModel.assign(dropTokenAction), GameModel.assign(switchPlayerAction)]
                }
              ]
            }
        },
        // VICTORY's state transitions
        [GameStates.VICTORY]: {
            on: {
                restart: { // in case of victory, redirect to the lobby state, or to play state also ?
                    target: GameStates.LOBBY,
                    actions: [GameModel.assign(restartAction)]
                }
            }
        },
        [GameStates.DRAW]: {
            on: {
                restart: { 
                    target: GameStates.LOBBY,
                    actions: [GameModel.assign(restartAction)]
                }
            }
        },
    }
})

export function makeGame (state: GameStates = GameStates.LOBBY, context: Partial<GameContext> = {}): InterpreterFrom<typeof GameMachine> {
  const machine = interpret(
    GameMachine.withContext({
      ...GameModel.initialContext,
      ...context
    })
  ).start()
  GameMachine
  machine.state.value = state
  return machine
}
