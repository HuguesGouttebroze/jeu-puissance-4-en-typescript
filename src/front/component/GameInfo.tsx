import { discColorClass } from '../../fnc/color'
import { PlayerColor } from '../../types'

type GameInfoProps = {
  color: PlayerColor,
  name: string
}

export function GameInfo ({color, name}: GameInfoProps) {
  return (
    <div>
      <h2 className="flex" 
          style={{gap: '.5rem'}}>
            
            This is to you, {name}, 
            <div className={discColorClass(color)}>
            </div> to play ! GO GO !</h2>
    </div> 
  )
}
