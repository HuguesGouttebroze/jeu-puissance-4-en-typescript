import { NameSelector } from "./screen/NameSelector";
import "./index.css";
import { PlayScreen } from './screens/PlayScreen'
import { GameStates, PlayerColor } from "../types";
import { Grid } from "./component/Grid";
import { VictoryScreen } from "./screens/VictoryScreen";
import { LobbyScreen } from "./screens/LobbyScreen";
import { useGame } from './hooks/useGame'
import { currentPlayer } from "../fnc/game";
import { DrawScreen } from "./screens/DrawScreen";

const GameTitle = "PUISSANCE 4";
const ProductBy = "by QwickQwack impl";

function App() {

 const {state, context, send} = useGame();
 const canDrop = state === GameStates.PLAY;
 const player = canDrop ? currentPlayer(context) : undefined;
 const dropToken = canDrop ? (x: number) => {
  send({ type: 'dropToken', x: x })
 } : undefined  
  
  return (
    <div className="container">   
      {state === GameStates.LOBBY && <LobbyScreen />}
      {state === GameStates.PLAY && <PlayScreen />}
      {state === GameStates.VICTORY && <VictoryScreen />}
      {state === GameStates.DRAW && <DrawScreen />}
      <Grid 
        winingPositions={context.winingPositions} 
        grid={context.grid}
        onDrop={dropToken} 
        color={player?.color} />
    </div>
  )
}

export default App
