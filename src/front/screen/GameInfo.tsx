import React from 'react'
import { discColorClass } from '../../fnc/color'
import { PlayerColor } from '../../types'

type GameInfoProps = {color: PlayerColor}

export function GameInfo({color}: GameInfoProps) {
  return (
    <div>
        <h2 className='flex' style={{gap: '.5rem'}}>
                       au tour de  <div className={ discColorClass(color)}></div>
                       de jouer !
         </h2> 
    </div>
  )
}