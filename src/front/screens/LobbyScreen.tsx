import { useGame } from '../hooks/useGame'
import { ColorSelector } from '../component/ColorSelector'
import { PlayerColor } from '../../types'
import { prevent } from '../../fnc/dom'
import { NameSelector } from '../component/NameSelector' 

type LobbyScreenProps = {}

export function LobbyScreen ({}: LobbyScreenProps) {
  
  const {send, context, can} = useGame()
  
  const colors = [PlayerColor.YELLOW, PlayerColor.RED]

  const joinGame = (name: string) => send({type: 'join', name: name, playerId: name})

  const chooseColor = (color: PlayerColor) => send({type: 'chooseColor', color, playerId: 
  color === PlayerColor.YELLOW ? 'Zoidberg' : 'QwickQwack'})
  
  const startGame = () => send({type: 'start'})

  const canStart = can({type: 'start'})

  return (
    <div>
      <NameSelector onSelect={joinGame} />
      <ColorSelector onSelect={chooseColor} 
        players={context.players} 
        colors={colors} 
      />
      <p>
        <button disabled={!canStart} className="button" onClick={prevent(startGame)}>Play!</button>
      </p>
    </div>
  )
}
