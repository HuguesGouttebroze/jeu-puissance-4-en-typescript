import { useGame } from '../hooks/useGame'
import { Draw } from '../component/Draw'

type DrawScreenProps = {}

export function DrawScreen ({}: DrawScreenProps) {
  const {send} = useGame()
  const restart = () => send({type: 'restart', playerId: 'Zoidberg'})
  return (
    <div>
      <h2>Oh! Equalitéééé!</h2>
      <Draw onRestart={restart} />
    </div>
  )
}
